typedef signed char        int8_t;
typedef short              int16_t;
typedef int                int32_t;
//typedef long long          int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
//typedef unsigned long long uint64_t;


typedef struct fileHeaderStruct {
	uint32_t RunNumber;
	uint32_t SamplingFrequency;
	uint32_t Presamples;
	uint32_t reserved[61];
} FileHeader;

typedef struct eventHeaderStruct {
	uint32_t Number;
	uint32_t Type;
	uint32_t Time;
	uint32_t reserved;
	uint32_t FPGAEvent;
	uint32_t FPGATime;
	uint32_t Channels;
	uint32_t Samples;
} EventHeader;

typedef struct eventAnalysisStruct {
	int32_t integral;
	int32_t maximum;
	int32_t pedestal;
	int32_t signal_start;
	int32_t PedestalDiff;
	int32_t PedestalSTDDEV;
	int32_t PedestalMaxVariation;
	int32_t Flag;
} eventAnalysis;